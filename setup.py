import setuptools
from setuptools import find_packages
setuptools.setup(
    name="test_utils",
    version="0.0.1",
    url='https://gitlab.com/Zawar-Ahmed-Tahir/test_utils.git',
    author="Zawar Ahmed Tahir",
    author_email="zawar.ahmed@tenpearls.com",
    description="Utility methods for ARC",
    python_requires='>=3.6',
    # packages=['arc_utils'],
    packages = find_packages(), 
)
